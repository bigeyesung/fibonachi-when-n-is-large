#include <iostream>
using namespace std;
const int MOD = 10000;

struct matrix // define matrix
{
    int m[2][2];
    
};

// matrix X matrix
matrix multi(matrix a,matrix b){
    matrix temp;
    for(int i=0;i<2;i++){
        for(int j=0;j<2;j++){
            temp.m[i][j]=0;
            for(int k=0;k<2;k++)
                temp.m[i][j] = (temp.m[i][j] + a.m[i][k]*b.m[k][j])%MOD;
        }
    }
    
    return temp;
}

// deal with the descending power
matrix Trans(matrix a, int n){
    matrix M;
    if(n==1){
        return a;
    }
    
  else if(n>=2){
    M = multi(Trans(a, n-1), a);
  }
    return M;
    
}



int main(int argc, const char * argv[]) {
    int n;
    cout<<"input number"<<endl;
    cin>>n;   // input n
    
    if(n==0) cout<<0;
    
    else if (n==1) cout<<1;
    
    else{
        // creating matrix T
        matrix t,k;
        t.m[0][0] = 1;
        t.m[0][1] = 1;
        t.m[1][0] = 1;
        t.m[1][1] = 0;
        
        k = Trans(t, n-1);
        int ans = k.m[0][0] *1 + k.m[0][1]*0;
        cout<<ans;

    
    }
    
    return 0;
}
